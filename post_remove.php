<?php
session_start();
$user = $_SESSION['user'];
if(!isset($user->type) || $user->type!='admin'){
    header("Location:/");
}
if(isset($_GET['id'])){
    include_once ("db.php");
    $db = new DB();
    $db->removePost((int)($_GET['id']));
}
header("Location:/");