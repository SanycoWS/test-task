<?php

class DB{
    private $host   =   'localhost';
    private $user   =   'root';
    private $pass   =   '';
    private $db     =   'test_task';
    private $conn;


    function __construct(){
        $this->conn = new mysqli();
        $this->conn->connect($this->host, $this->user, $this->pass, $this->db);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

    private function query($query){
        $res = $this->conn->query($query);
        if(!$res){
            echo $this->conn->errno." ".$this->conn->error."\n";
            return false;
        }
        return $res;
    }

    function getPosts(){
        $query = "SELECT * FROM `posts` ";
        $result = $this->query($query);
        $res = [];
        if($result){
            while ($myrow = $result->fetch_object()){
                $res[] = $myrow;
            }
            $result->free();
        }
        return $res;
    }

    function getComments($post_id){
        $query = "SELECT `comments`.*,`users`.`login` FROM `comments` INNER JOIN `users` on `users`.`id`=`comments`.`user_id` WHERE `post_id`=".$post_id." ORDER BY `date` DESC";
        $result = $this->query($query);
        $res = [];
        if($result){
            while ($myrow = $result->fetch_object()){
                $res[] = $myrow;
            }
            $result->free();
        }
        return $res;
    }

    function getPostInfo($id){
        $query = "SELECT * FROM `posts` WHERE `id`=".$id." LIMIT 1";
        $result = $this->query($query);

        if($result){
            while ($myrow = $result->fetch_object()){
                return $myrow;
            }
            $result->free();
        }
        return false;
    }

    function tryLogin($login,$password){
        $query = "SELECT * FROM `users` WHERE `login`='".$this->conn->escape_string($login)."' limit 1 ";
        $result = $this->query($query);
        if($result){
            while ($myrow = $result->fetch_object()){
                if(password_verify($this->conn->escape_string($password), $myrow->password)) {
                    return $myrow;
                }else{
                    return false;
                }
            }
            $result->free();
        }
        return false;
    }

    function removePost($id){
        $query = "DELETE FROM `posts` WHERE `id`='".$id."';";
        $this->query($query);
    }

    function removeComment($id){
        $query = "UPDATE `comments` SET `status`='delete' WHERE `id`='".$id."';";
        $this->query($query);
    }

    function createPost($name,$description){
        $query = "INSERT INTO `posts` (`name`, `desc`,`date`) 
        VALUES ('".$this->conn->escape_string($name)."', '".$this->conn->escape_string($description)."', '".date("Y-m-d H:i:s")."');";
        $this->query($query);
    }

    function addComment($comment,$post_id,$user_id){
        $query = "INSERT INTO `comments` (`post_id`, `user_id`, `date`, `text`) 
        VALUES ('".(int)($post_id)."', '".(int)($user_id)."', '".date("Y-m-d H:i:s")."', '".$this->conn->escape_string($comment)."');";
        $this->query($query);
    }

    function registration($login,$password){
        $query = "INSERT INTO `users` (`login`, `password`) 
        VALUES ('".$this->conn->escape_string($login)."', '".password_hash($this->conn->escape_string($password), PASSWORD_DEFAULT)."');";
        $this->query($query);
    }
}
?>