<a href="#" data-toggle="modal" data-target="#loginFormModal">login</a>
<!-- Modal -->
<div class="modal fade" id="loginFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Form</h4>
            </div>
            <div class="modal-body">
                <form action="login.php" method="post" onsubmit="return false;">
                    <div class="form-group">
                        <label for="inputLogin">Email address</label>
                        <input type="email" class="form-control" id="inputLogin" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="loginBtnSubmit">Login</button>
            </div>
        </div>
    </div>
</div>

<a href="#" data-toggle="modal" data-target="#registrationFormModal">registration</a>
<!-- Modal -->
<div class="modal fade" id="registrationFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Registration Form</h4>
            </div>
            <div class="modal-body">
                <form action="registration.php" method="post" onsubmit="return false;">
                    <div class="form-group">
                        <label for="inputLogin">Email address</label>
                        <input type="email" class="form-control" id="inputLogin" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="registrationBtnSubmit">Registration</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#loginBtnSubmit").on("click",function(){
            $.post('/login.php',{
                login:$("#loginFormModal #inputLogin").val(),
                password:$("#loginFormModal #inputPassword").val()
            },function(result){
                var data = $.parseJSON(result);
                if(data.status){
                    window.location.reload();
                }else{
                    alert("bad login or password");
                }
            })
        });

        $("#registrationBtnSubmit").on("click",function(){
            $.post('/registration.php',{
                login:$("#registrationFormModal #inputLogin").val(),
                password:$("#registrationFormModal #inputPassword").val()
            },function(result){
                var data = $.parseJSON(result);
                if(data.status){
                    window.location.reload();
                }else{
                    alert("bad login or password");
                }
            })
        });
    });
</script>