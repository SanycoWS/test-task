<?php
session_start();
$user = $_SESSION['user'];
if(!isset($user->type) || $user->type!='admin'){
    header("Location:/");
}
if(isset($_POST['id'])){
    include_once ("db.php");
    $db = new DB();
    $db->removeComment((int)($_POST['id']));
}
