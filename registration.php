<?php
if(isset($_POST['login']) && isset($_POST['password'])){
    include_once ('db.php');
    $db = new DB();
    $login = trim($_POST['login']);
    $password = trim($_POST['password']);
    $result = $db->registration($login,$password);
    if(!$result){
        session_start();
        $_SESSION['user'] = $db->tryLogin($login,$password);
        echo json_encode(['status'=>true]);
    }else{
        echo json_encode(['status'=>false]);
    }
}