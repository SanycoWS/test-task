<?php
include_once ('DB.php');
session_start();
$user = $_SESSION['user'];
if(isset($user->id) && isset($_POST['comment']) && isset($_POST['post'])){
    $db = new DB();
    if(strlen($_POST['comment'])>0) {
        $db->addComment($_POST['comment'],$_POST['post'],$user->id);
        $comment = [
            'login'   => $user->login,
            'date'    => date("Y-m-d H:i:s"),
            'text'    => $_POST['comment'],
            'status'  => 'active'
        ];
        $comment = (object)($comment);
        include_once ('comments_view.php');
    }
}