CREATE TABLE `comments` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`post_id` INT(11) NULL DEFAULT NULL,
	`user_id` INT(11) NULL DEFAULT NULL,
	`date` DATETIME NULL DEFAULT NULL,
	`text` TEXT NULL,
	`status` ENUM('active','delete') NULL DEFAULT 'active',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `posts` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT NULL,
	`desc` TEXT NULL,
	`date` DATETIME NULL DEFAULT NULL,
	`status` ENUM('active','delete') NULL DEFAULT 'active',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=6
;
CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`login` VARCHAR(50) NULL DEFAULT NULL,
	`password` VARCHAR(255) NULL DEFAULT NULL,
	`type` ENUM('admin','user') NULL DEFAULT 'user',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;
