<?php
if(isset($_POST['login']) && isset($_POST['password'])){
    include_once ('db.php');
    $db = new DB();
    $login = trim($_POST['login']);
    $password = trim($_POST['password']);
    $user = $db->tryLogin($login,$password);
    if($user!==false){
        session_start();
        $_SESSION['user'] = $user;
        echo json_encode(['status'=>true]);
    }else{
        echo json_encode(['status'=>false]);
    }
}