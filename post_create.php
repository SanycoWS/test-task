<?php
include_once ('DB.php');
include_once ('header.php');
if(!isset($user->type) || $user->type!='admin'){
    header("Location:/");
}
include_once ('post_create_view.php');
include_once ('footer.php');