<div class="row">
    <div class="col-sm-8"><textarea class="form-control" id="addCommentData" placeholder="comment"></textarea></div>
    <div class="col-sm-4"><button class="btn btn-primary" id="addCommentBtn">Add Comment</button></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#addCommentBtn").on("click",function(){
            $.post('/add_comment.php',{comment:$("#addCommentData").val(),post:<?=$post->id?>},function(result){
                $("#comments").prepend(result);
            });
            $("#addCommentData").val('');
        });
        <?php if(isset($user->type) && $user->type=='admin'){?>
            $(".removeComment").on('click',function(){
                var id = $(this).attr('comment_id');
                $.post('comment_remove.php',{id:id},function(){
                    $(".comment_item_"+id+" .text").html('removed');
                })
            });
        <?}?>
    })
</script>