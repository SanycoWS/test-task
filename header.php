<?php
    session_start();
    $user = $_SESSION['user'];
?>
<html>
    <head>
        <title></title>
        <script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <?php
                if(isset($user->login)){
                    include("login_view.php");
                }else{
                    include("login_quest_view.php");
                }

            ?>
